Unless otherwise licensed, music in this repo is under the [MIT (Expat) License](https://opensource.org/licenses/MIT), a copy of which is included with the repo.

Otherwise licensed music will have the copyright information at the bottom of the PDF or `.mscz` when imported into [MuseScore](https://musescore.org/en), the notation software I use.
